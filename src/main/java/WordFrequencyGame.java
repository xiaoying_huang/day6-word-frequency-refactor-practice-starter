import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputString) {
            try {
                List<WordFrequency> valueCountList = getValueCountList(inputString);
                Map<String, List<WordFrequency>> wordFrequencyGroupByWord = getWordFrequencyGroupByWord(valueCountList);
                List<WordFrequency> wordFrequencyWithCount = setWordFrequencyWithCount(wordFrequencyGroupByWord);
                List<WordFrequency> sortedWordFrequency = sortWordFrequencyByCount(wordFrequencyWithCount);
                return formatWordFrequency(sortedWordFrequency);
            } catch (Exception e) {
                return CALCULATE_ERROR;
            }
    }

    private static String formatWordFrequency(List<WordFrequency> sortedWordFrequency) {
        return sortedWordFrequency.stream()
                .map(wordFrequency -> wordFrequency.getValue() + " " + wordFrequency.getWordCount())
                .collect(Collectors.joining("\n"));
    }

    private static String getResult(StringJoiner joiner) {
        return joiner.toString();
    }

    private static List<WordFrequency> sortWordFrequencyByCount(List<WordFrequency> expectedValueCounts) {
        expectedValueCounts.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return expectedValueCounts;
    }

    private static List<WordFrequency> setWordFrequencyWithCount(Map<String, List<WordFrequency>> wordFrequencyGroupByWord) {
        return  wordFrequencyGroupByWord
                .entrySet()
                .stream()
                .map(entry-> new WordFrequency(entry.getKey(), entry.getValue().size()))
                .collect(Collectors.toList());
    }

    private List<WordFrequency> getValueCountList(String inputString) {
        String[] singleWords = inputString.split("\\s+");
        List<String> singleWordList = Arrays.stream(singleWords).collect(Collectors.toList());

        return singleWordList.stream()
                .map(singleWord -> new WordFrequency(singleWord, 1))
                .collect(Collectors.toList());
    }

    private Map<String, List<WordFrequency>> getWordFrequencyGroupByWord(List<WordFrequency> valueCountList) {
        Map<String, List<WordFrequency>> valueValueCounts = new HashMap<>();
        for (WordFrequency valueCount : valueCountList){
            valueValueCounts.computeIfAbsent(valueCount.getValue(), k -> new ArrayList<>()).add(valueCount);
        }
        return valueValueCounts;
    }
}
